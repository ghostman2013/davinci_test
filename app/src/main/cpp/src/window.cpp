//
// Created by ConteDevel on 19.07.18.
//

#include <android_native_app_glue.h>
#include "window.h"

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "window", __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "window", __VA_ARGS__))

using namespace DaVinci;

Window::Window(struct android_app *app) : m_app{app} {}
Window::~Window() {}

struct android_app *Window::app() const { return m_app; }
EGLDisplay Window::display() const { return m_display; }
EGLSurface Window::surface() const { return m_surface; }
EGLContext Window::context() const { return m_context; }
EGLint Window::width() const { return m_width; }
EGLint Window::height() const { return m_height; }

void Window::display(EGLDisplay d) { m_display = d; }
void Window::surface(EGLSurface s) { m_surface = s; }
void Window::context(EGLContext ctx) { m_context = ctx; }
void Window::width(EGLint w) { m_width = w; }
void Window::height(EGLint h) { m_height = h; }

int Window::init() {
    /*
     * Here specify the attributes of the desired configuration.
     * Below, we select an EGLConfig with at least 8 bits per color
     * component compatible with on-screen windows
     */
    const EGLint attrs[] = {
            EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
            EGL_BLUE_SIZE, 8,
            EGL_GREEN_SIZE, 8,
            EGL_RED_SIZE, 8,
            EGL_NONE
    };
    EGLint format;
    EGLint numConfigs;
    EGLConfig config;

    m_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    eglInitialize(m_display, 0, 0);
    /* Here, the application chooses the configuration it desires.
     * find the best match if possible, otherwise use the very first one
     */
    eglChooseConfig(m_display, attrs, nullptr, 0, &numConfigs);
    std::unique_ptr<EGLConfig[]> supportedConfigs(new EGLConfig[numConfigs]);
    assert(supportedConfigs);
    eglChooseConfig(m_display, attrs, supportedConfigs.get(), numConfigs, &numConfigs);
    assert(numConfigs);
    auto i = 0;
    for (; i < numConfigs; i++) {
        auto& cfg = supportedConfigs[i];
        EGLint r, g, b, d;
        if (eglGetConfigAttrib(m_display, cfg, EGL_RED_SIZE, &r)   &&
            eglGetConfigAttrib(m_display, cfg, EGL_GREEN_SIZE, &g) &&
            eglGetConfigAttrib(m_display, cfg, EGL_BLUE_SIZE, &b)  &&
            eglGetConfigAttrib(m_display, cfg, EGL_DEPTH_SIZE, &d) &&
            r == 8 && g == 8 && b == 8 && d == 0 ) {

            config = supportedConfigs[i];
            break;
        }
    }
    if (i == numConfigs) {
        config = supportedConfigs[0];
    }

    /* EGL_NATIVE_VISUAL_ID is an attribute of the EGLConfig that is
     * guaranteed to be accepted by ANativeWindow_setBuffersGeometry().
     * As soon as we picked a EGLConfig, we can safely reconfigure the
     * ANativeWindow buffers to match, using EGL_NATIVE_VISUAL_ID. */
    eglGetConfigAttrib(m_display, config, EGL_NATIVE_VISUAL_ID, &format);
    m_surface = eglCreateWindowSurface(m_display, config, m_app->window, NULL);
    m_context = eglCreateContext(m_display, config, NULL, NULL);

    if (eglMakeCurrent(m_display, m_surface, m_surface, m_context) == EGL_FALSE) {
        LOGW("Unable to eglMakeCurrent");
        return -1;
    }

    eglQuerySurface(m_display, m_surface, EGL_WIDTH, &m_width);
    eglQuerySurface(m_display, m_surface, EGL_HEIGHT, &m_height);

    // Check openGL on the system
    auto opengl_info = {GL_VENDOR, GL_RENDERER, GL_VERSION, GL_EXTENSIONS};
    for (auto name : opengl_info) {
        auto info = glGetString(name);
        LOGI("OpenGL Info: %s", info);
    }
    // Initialize GL state.
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST);
    glEnable(GL_CULL_FACE);
    glShadeModel(GL_SMOOTH);
    glDisable(GL_DEPTH_TEST);

    return 0;
}

void Window::destroy() {
    if (m_display != EGL_NO_DISPLAY) {
        eglMakeCurrent(m_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        if (m_context != EGL_NO_CONTEXT) {
            eglDestroyContext(m_display, m_context);
        }
        if (m_surface != EGL_NO_SURFACE) {
            eglDestroySurface(m_display, m_surface);
        }
        eglTerminate(m_display);
    }
    m_display = EGL_NO_DISPLAY;
    m_context = EGL_NO_CONTEXT;
    m_surface = EGL_NO_SURFACE;
}

void Window::swapBuffers() { eglSwapBuffers(m_display, m_surface); }
bool Window::ready() { return m_app->window != nullptr; }