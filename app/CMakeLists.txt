# For more information about using CMake with Android Studio, read the
# documentation: https://d.android.com/studio/projects/add-native-code.html

cmake_minimum_required(VERSION 3.4.1)

set(CMAKE_VERBOSE_MAKEFILE on)

# build native_app_glue as a static lib
set(${CMAKE_C_FLAGS}, "${CMAKE_C_FLAGS}")
include_directories(${ANDROID_NDK}/sources/android/native_app_glue)
add_library(app_glue STATIC ${ANDROID_NDK}/sources/android/native_app_glue/android_native_app_glue)

# now build app's shared lib
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Werror")
# Export ANativeActivity_onCreate(),
# Refer to: https://github.com/android-ndk/ndk/issues/381.
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -u ANativeActivity_onCreate")

file(GLOB_RECURSE source_list "src/main/cpp/src/*.cpp")
add_library(davinci SHARED "${source_list}")

target_include_directories(davinci PRIVATE
    "${ANDROID_NDK}/sources/android/native_app_glue"
    "${ANDROID_NDK}/sysroot/usr/include"
    "src/main/cpp/include")

# add lib dependencies
target_link_libraries(
    davinci
    android
    log
    m
    app_glue
    EGL
    GLESv1_CM
    camera2ndk
    mediandk
)