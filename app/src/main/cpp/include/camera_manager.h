//
// Created by ConteDevel on 21.07.18.
//

#ifndef DAVINCI_CAMERA_MANAGER_H
#define DAVINCI_CAMERA_MANAGER_H

#include <map>
#include <string>
#include <camera/NdkCameraManager.h>
#include <camera/NdkCameraError.h>
#include <camera/NdkCameraDevice.h>
#include <camera/NdkCameraMetadataTags.h>
#include <media/NdkImageReader.h>

namespace DaVinci {
    class CameraId;

    class CameraManager {
        struct ACameraManager *_manager;
        std::map<std::string, CameraId> _cameras;
        std::string _activeCameraId;

        int32_t _cameraFacing;
        int32_t _cameraOrientation;

        bool _valid;

    public:
        CameraManager();
        ~CameraManager();
    };

    // helper classes to hold enumerated camera
    class CameraId {
    public:
        struct ACameraDevice *device;
        std::string id;
        acamera_metadata_enum_android_lens_facing_t facing;
        bool available;  // free to use ( no other apps are using
        bool owner;      // we are the owner of the camera

        explicit CameraId(const char *id);
        explicit CameraId();
    };
};

#endif //DAVINCI_CAMERA_MANAGER_H
