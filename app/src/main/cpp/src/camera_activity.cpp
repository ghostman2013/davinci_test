#include <initializer_list>
#include <memory>
#include <cstdlib>
#include <cstring>
#include <jni.h>
#include <dlfcn.h>

#include <EGL/egl.h>
#include <GLES/gl.h>

#include <android/sensor.h>
#include <android/log.h>
#include <android_native_app_glue.h>

#include "window.h"
#include "camera_manager.h"

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "camera-activity", __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "camera-activity", __VA_ARGS__))

/**
 * Just the current frame in the display.
 */
static void engine_draw_frame(DaVinci::Window *wnd) {
    if (wnd->display() == NULL) { return; }
    glClearColor(1, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT);
    wnd->swapBuffers();
}

/**
 * Process the next main command.
 */
static void engine_handle_cmd(struct android_app *app, int32_t cmd) {
    auto wnd = (DaVinci::Window *)app->userData;
    switch (cmd) {
        case APP_CMD_INIT_WINDOW:
            // The window is being shown, get it ready.
            if (wnd->ready()) {
                wnd->init();
                engine_draw_frame(wnd);
            }
            break;
        case APP_CMD_TERM_WINDOW:
            // The window is being hidden or closed, clean it up.
            wnd->destroy();
            break;
    }
}

/**
 * This is the main entry point of a native application that is using
 * android_native_app_glue.  It runs in its own thread, with its own
 * event loop for receiving input events and doing other things.
 */
void android_main(struct android_app* state) {
    DaVinci::Window wnd(state);
    state->userData = &wnd;
    state->onAppCmd = engine_handle_cmd;

    DaVinci::CameraManager cameraManager;

    // loop waiting for stuff to do.
    while (1) {
        // Read all pending events.
        int events;
        struct android_poll_source* source;

        // If not animating, we will block forever waiting for events.
        // If animating, we loop until all events are read, then continue
        // to draw the next frame of animation.
        while (ALooper_pollAll(0, NULL, &events, (void**)&source) >= 0) {
            // Process this event.
            if (source != NULL) {
                LOGI("New event");
                source->process(state, source);
            }
            // Check if we are exiting.
            if (state->destroyRequested != 0) {
                wnd.destroy();
                return;
            }
        }
    }
}