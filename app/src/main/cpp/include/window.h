//
// Created by ConteDevel on 19.07.18.
//

#ifndef DAVINCI_WINDOW_H
#define DAVINCI_WINDOW_H

#include <memory>

#include <EGL/egl.h>
#include <GLES/gl.h>

#include <android/log.h>

namespace DaVinci {
    class Window {
        struct android_app *m_app;
        EGLint m_width = 0;
        EGLint m_height = 0;
        EGLDisplay m_display = EGL_NO_DISPLAY;
        EGLSurface m_surface = EGL_NO_SURFACE;
        EGLContext m_context = EGL_NO_CONTEXT;
    public:
        Window(struct android_app *app = nullptr);
        ~Window();
        // Getters
        struct android_app *app() const;
        EGLDisplay display() const;
        EGLSurface surface() const;
        EGLContext context() const;
        int32_t width() const;
        int32_t height() const;
        // Setters
        void display(EGLDisplay d);
        void surface(EGLSurface s);
        void context(EGLContext ctx);
        void width(EGLint w);
        void height(EGLint h);
        // Methods
        int init();
        void destroy();
        void swapBuffers();
        bool ready();
    };
};


#endif //DAVINCI_WINDOW_H
