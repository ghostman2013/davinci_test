//
// Created by ConteDevel on 21.07.18.
//

#include "camera_manager.h"

using namespace DaVinci;

CameraManager::CameraManager() : _manager(nullptr), _activeCameraId(""),
                                 _cameraFacing(ACAMERA_LENS_FACING_BACK),
                                 _cameraOrientation(0) {
    _valid = false;
}

CameraManager::~CameraManager() {}

CameraId::CameraId(const char *id) : device(nullptr), facing(ACAMERA_LENS_FACING_FRONT),
                                     available(false), owner(false), id(id) {}
CameraId::CameraId() : id("") {}

