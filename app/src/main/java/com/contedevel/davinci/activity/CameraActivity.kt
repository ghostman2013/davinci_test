package com.contedevel.davinci.activity

import android.app.NativeActivity
import android.os.Bundle
import android.view.View
import com.contedevel.davinci.R





class CameraActivity : NativeActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_main)

        val sdkInt = android.os.Build.VERSION.SDK_INT
        if (sdkInt >= 19) {
            setImmersiveSticky()
            val decorView = window.decorView
            decorView.setOnSystemUiVisibilityChangeListener { setImmersiveSticky() }
        }

        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        //Hide toolbar
        val sdkInt = android.os.Build.VERSION.SDK_INT
        when {
            sdkInt in 11..13 -> window.decorView.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LOW_PROFILE
            sdkInt in 14..18 -> window.decorView.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_LOW_PROFILE
            sdkInt >= 19 -> setImmersiveSticky()
        }

        super.onResume()
    }

    fun setImmersiveSticky() {
        val decorView = window.decorView
        decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE)
    }
}